
/*  
          En las siguientes lineas se importa las dependencias : 
          
          'useState' y 'useEffect' para manejar los datos de la tabla, en los array que contienen datos 
          y respectivamente se mostrara en la tabla.  Nos permite declar variables, actualizarlas dentro 
          de una funcion y que realize solicitudes a una API. 

          'datatable' nos permite implementar el manejo de tables para mostrar los datos de manera
          organizada.

*/


/*        declaramos un componente funcional, la misma que nos permite que se devuelvan los componentes\
          dentro de esta declaramos los estados "userState y useEffect" para almancenar los datos deL Pokemon,
          el 'londing' indica que los datos se estan cargando y se inicia con un 'true'.

*/
import React, { useState, useEffect } from 'react';
import DataTable from 'react-data-table-component';

export default function PokemonTable() {
  const [pokemonData, setPokemonData] = useState([]);
  const [loading, setLoading] = useState(true);

  useEffect(() => {
    fetch('https://pokeapi.co/api/v2/pokemon')
      .then(response => response.json())
      .then(data => {
        setPokemonData(data.results);
        setLoading(false);
      })
      .catch(error => {
        console.error('Error fetching data:', error);
        setLoading(false);
      });
  }, []);


  /*
  se define las columnas para proporcionar los datos en el
  componente dataTable
  */
  const columns = [
    {
      name: 'Name',
      selector: 'name',
      sortable: true,
    },
    {
      name: 'URL',
      selector: 'url',
      sortable: true,
    },
  ];

  /*

  Utiliza el componente DataTable del paquete
  react-data-table-component para presentar
  los datos en una tabla.

  Permite configurar la apariencia y el comportamiento de la tabla utilizando
  propiedades como progressPending, noHeader, dense, striped y pagination.
  
  */
  return (
    <div>
      <h1>Pokémon Data</h1>
      <DataTable
        columns={columns}
        data={pokemonData}
        progressPending={loading}
        noHeader
        dense
        striped
        pagination
      />
    </div>
  );
}