import logo from './logo.svg';
import './App.css';
import PokemonTable from './components/PokemonTable';

function App() {
  return (
    <div className="App">
<PokemonTable/>
    </div>
  );
}

export default App;
