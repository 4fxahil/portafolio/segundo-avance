

// Formulario.

/* 

     Para el formulario en react importamos la dependencia de "useState"
     useState, permite manejar datos sin que este tenga que convertirse
     en componentes de clase.

*/

import React, { useState } from 'react';

export default function Form(){
    
    
    /* 
    Se asignan variables con un valor inicial con una 
    cadena vacia, y una funcion que permite actualizar 
    el estado.
   */
    const [firstName, selfFirstName]= useState('');
    const [lastName, selfLastName] = useState('');

    const fullname = firstName + '' + lastName

    function handleFirstNameChange(e){
        selfFirstName(e.target.value); 
        
    }

    function handleLastNameChange(e){
        selfLastName(e.target.value);
    }

/* 
Se define un formulario con la clase CSS "form"
y que este establecaz los valores de los campos
que se llenaran los estados y se veran
reflejados, y que cuando se cambie el contenido
se escifique en su entrada para que pueda retornar
las funciones.
*/

    return(
        <>
        
        <h2>SignUp</h2>
        <form action="" class="form">

        <label htmlFor="firstName">First Name : </label>
        <input type="text" value={firstName} onChange={handleFirstNameChange}/>

        <label htmlFor="lasName">Last Name : </label>
        <input type="text" value={lastName} onChange={handleLastNameChange}/>

        </form>

        <p> Welcome : <strong>{fullname}</strong></p>
        
        </>
    );
}