//Tablas
/* 
Agregamos un componente para 
insertar una tabla a nuestra App.
Esta tabla consiste en 3x3, tres columnas
y 3 filas. 
*/

export default function Table3x3(props) {
    return(
        <table>
            <thead>
                <th>{props.th1} </th>
                <th>{props.th2} </th>
                <th>{props.th3} </th>
            </thead>
            <tbody>
                <tr>
                <th>Cell 1 </th>
                <th>Cell 2 </th>
                <th>Cell 3 </th> 
                </tr>
                <tr>
                <th>Cell 4 </th>
                <th>Cell 5 </th>
                <th>Cell 6 </th> 
                </tr>
                <tr>
                <th>Cell 7 </th>
                <th>Cell 8 </th>
                <th>Cell 9 </th> 
                </tr>
            </tbody>
        </table>
    )
}