//Textos
/* 

Este componente es solo para regresar
y almacenar informacion (textos) junto con
su propio CSS
*/

export default function Text(props){
    return(
            <p style={styles.colorText}>{props.p}</p>
    )
}

const styles = {
    colorText:{
        color :  "lightgreen",
        textAlign: "left",
        fontSize : 64,
        fontFamily: "Roboto, Helvetica",
    },
};

