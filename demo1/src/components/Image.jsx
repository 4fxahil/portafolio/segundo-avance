//Imagenes
/* 

Este es el componente de imagenes, utilizamos
la propiedad de props, para pasar informacion
en este caso, para poder pasar imagenes
a la aplicacion. 

Junto con un CSS propio, para modificar
su altura, anchura y posicion. 
*/

import pony1 from '../assets/images/pony1.png';

export default function Image(props){
    return(
        <img style={styles.reSize} src ={pony1}/>
    )

};

const styles = {
    reSize:{
        width: 320,
        height: 340,
        marginLeft: 400,
        

    },
};