// Carpeta components : Se agregan todos los componentes de la aplicacion, para retomarlas en la app principal.
// Carpeta assets : Se crea la carpeta de imagenes, para almacenar las mismas. 

import logo from './logo.svg';
import './App.css';

// Importacion de componentes. 

/* 

     Se importa los componentes a la app.js principal, en donde se mostrara, 
     regresando todas las funciones de todos los componentes.

*/

import Image from './components/Image';
import Welcome from './components/Welcome';
import Text from './components/Text';
import Table3x3 from './components/Table3x3';
import Form from './components/Form';


import Header from './components/Header';
import CarouselDefault from './components/CarouselDefault';
import Cards from './components/Cards';
import  Footer  from './components/Footer';


// Function App.

/* 

     Funcion que regresa las interfaces.

*/

export default function App() {
  return (

<div>
<Header/>
<br/>

<Welcome name=" Fluttershy"/>
<Welcome msg="is a pony the type pegaso"/>
<Text p="holi poli boli "/>
<Image/>
<Table3x3  th1={1} th2={2} th3={3}/>
<Form/>

<CarouselDefault/>
<Cards/>
<Footer/>
</div>
  );

}
