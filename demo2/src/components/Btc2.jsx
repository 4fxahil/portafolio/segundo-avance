//Api

// Obtener los datos de las monedas

/* 
El siguiente codigo muestra las importaciones de
useState y useEffect que se utilizan
para manejar los estados que se importan.

*/

import { useState, useEffect } from 'react';
import '../App.css';

/* 
Se define la URL de la API con la que extraeremos informacion

*/

const url = "https://api.coindesk.com/v1/bpi/currentprice.json"

/*
Se define el componente de funcion, la que se encargara
para almacenar los datos de la API
Se indica el Fetch, que obtiene los datos de la URL, para 
convertirlos en formato JSON.
*/
export default function Btc2() {
  const[data, SetData] = useState(null);
  const [isLoading, setIsLoading] = useState(true);
  const [error, setError] = useState(null);

  useEffect(() => {
    fetch(url)
      .then(response => response.json())
      .then(result => {
        setIsLoading(false);
        SetData(result);
      }, error => {
        setIsLoading(false);
        setError(error);
      })
  },[]);


  /* 
  Si hay un error, muestra un mensaje de error.
   Si no hay errores y los datos están cargados,
  mostrara una tabla con los detalles de las monedas.
  */

  const getContent = () => {
    if (isLoading) {
      return (
        <div className="App">
          <h4>Loading Data ...</h4>
          <progress value={null} />
        </div>
      );
    }
  

    if (error) {
      return <h4>error</h4>
    }

  
/*
Estos datos de regresaran para mostrarlos en una 
tabla, juntos sus parametros y atributos.
*/
    return (
      <div className="App">
        <h1>BTC to USD |EUR |GBP</h1>
        <h3>BTC to USD</h3>


        <div class = "col-2">

        </div>


        <div class = "col-8">
          <table class = 'table table-striped'>
            <thead>
              <th>RATE</th>
              <th>RATE FLOAT</th>
              <th>DESCRIPTION</th>
              <th>UPDATE</th>
            </thead>
            <tbody>
              <tr>
                <td>{data["bpi"]["USD"].rate}</td>
                <td>{data["bpi"]["USD"].rate_float}</td>
                <td>{data["bpi"]["USD"].description}</td>
                <td>{data["time"].updated}</td>
              </tr>
              <tr>
                <td>{data["bpi"]["EUR"].rate}</td>
                <td>{data["bpi"]["EUR"].rate_float}</td>
                <td>{data["bpi"]["EUR"].description}</td>
                <td>{data["time"].updated}</td>
              </tr>
              <tr>
                <td>{data["bpi"]["GBP"].rate}</td>
                <td>{data["bpi"]["GBP"].rate_float}</td>
                <td>{data["bpi"]["GBP"].description}</td>
                <td>{data["time"].updated}</td>
              </tr>
            </tbody>
          </table>
        </div>


        <div class= "col-2">

        </div>
        
      </div>
    );
  }

  console.log(data)

  return (
    <div className="App">
      {getContent()}
    </div>
  );

}